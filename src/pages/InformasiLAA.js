import React, { Component, useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    View,
    Image
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, TableWrapper, Row, Rows, Cell } from 'react-native-table-component';
import axios from 'axios';
import { config } from '../config';

const App = (props) => {
    const { navigation } = props;
    const tableHead = ['No.', 'Nama Perusahaan', 'Alamat Perusahaan', 'Kontak Perusahaan','Opsi'];
    const [tableData, setTableData] = useState([
        ['No.', 'Nama Perusahaan', 'Alamat Perusahaan', 'Kontak Perusahaan', 'Opsi']
    ]);
    const [table, setTable] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/perusahaan-rekruitasi',
            data: {
                
            }
        }).then((response) => {
            var data = response.data.data;
            var temp = [];
            data.forEach((item, index) => {
                var row = [index+1, item.nama_perusahaan,item.alamat_lkp, item.kontak_perusahaan, element(item.nama_perusahaan,item.alamat_lkp, item.kontak_perusahaan)];
                temp.push(row);
            });
            setTableData(temp);
            setTable(true);
        });
    }

    const _alertIndex = (nama_perusahaan,alamat_lkp,kontak_perusahaan) => {
        Alert.alert(
            'Delete',
            nama_perusahaan,
            [
                { text: 'Cancel', onPress: () => { } },
                { text: 'OK', onPress: () => console.log("OK") },
            ],
            { cancelable: false }
        )
    }

    const element = (nama_perusahaan,alamat_lkp,kontak_perusahaan) => (
        <TouchableOpacity onPress={() => _alertIndex(nama_perusahaan, alamat_lkp, kontak_perusahaan)}>
            <View style={styles.btn}>
                <Text style={styles.btnText}>Delete</Text>
            </View>
        </TouchableOpacity>
    );
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC', flex: 1 }}>
            <View style={styles.boxHeader}>
                <Text style={styles.boxHeadertxt1}>Dashboard {'>'} Perusahaan Rekruitasi</Text>
            </View>
            <View style={styles.boxBottom}>
                <View style={styles.headerboxWhite}>
                    <Text style={{ fontSize:16,color:'#24305A', alignSelf: 'center' }}>List Perusahaan Rekruitasi</Text>
                    <View style={{marginHorizontal:'10%',marginVertical:10 }}>
                    <Button
                        //onPress={() => handleSignIn()}
                        title="Tambah Perusahaan"
                        color="#24305A"
                    />
                    </View>
                    <View><TextInput placeholder='Search' placeholderTextColor="grey" style={{ alignSelf:'center',borderWidth: 1, width: '80%', paddingVertical: 0, borderColor: '#A8AAB0' }} /></View>
                </View>

                <ScrollView>
                    <ScrollView horizontal={true}>
                        <Table>
                            <Row data={tableHead} widthArr={[50, 200, 350, 200, 80]} style={styles.head} textStyle={styles.textHead} />

                            {
                                tableData.map((rowData, index) => (
                                    <Row
                                        key={index}
                                        data={rowData}
                                        style={[index % 2 && { backgroundColor: '#EFEEEF' }]}
                                        textStyle={styles.text}
                                        widthArr={[50, 200, 350, 200,80]}
                                    />
                                ))
                            }

                        </Table>

                    </ScrollView>

                </ScrollView>
            </View>
            <View style={{ position: 'relative', paddingHorizontal: '5%', paddingVertical: '2%', marginTop: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    textData: {
        alignSelf: 'center',
        marginBottom: 10,
        color: '#24305A'
    },
    boxHeader: {
        zIndex: -1,
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        marginBottom: '122%'
    },
    boxBottom: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        marginTop: '15%',
        alignSelf: 'center',
        height: '85%'
    },
    head: {
        height: 40,
        backgroundColor: '#24305A',
        alignItems:'center'
    },
    text: {
        margin: 6,
        color: 'black',
        
        marginBottom: 10
    },
    textHead: {
        color: 'white',
        margin:6
    },
    btn: {
        width: 58, height: 20, backgroundColor: '#24305A', borderRadius: 2,
        margin:6
    },
    btnText: {
        textAlign: 'center', color: 'white'
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 16,
        marginLeft: '5%',
        marginBottom: '30%',
        marginTop: '2%'
    },
    headerboxWhite: {
        marginBottom: 20
    }
});

export default App;