import React, { useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Alert,
    _ScrollView
} from 'react-native';
import { useRoute } from "@react-navigation/native";
import { Colors } from 'react-native/Libraries/NewAppScreen';
import axios from 'axios';
import { config } from '../config';
const App = (props) => {
    const submitButtonAlert = () =>
        Alert.alert(
            "REMINDER!",
            "Anda yakin dengan data yang di-inputkan?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: 'cancel'
                },
                {
                    text: "OK", onPress: () => navigation.navigate("NilaiAkademik"),
                },

            ],
        );
    const { navigation } = props;
    const { params } = useRoute();
    const [clo_1_akd, setclo_1_akd] = useState();
    const [clo_4a_akd, setclo_4a_akd] = useState();
    const [clo_4b_akd, setclo_4b_akd] = useState();
    const handleSubmit = () => {
        var clo_4_akd = ((parseInt(clo_4a_akd) + parseInt(clo_4b_akd)));
        axios({
            method: 'post',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            url: config.system.url+'/index.php/api/edit-nilai-akd',
            data: 'nim=' + params.nim + '&clo_1_akd=' + clo_1_akd + '&clo_4_akd=' + clo_4_akd 
        }).then((response) => {
            var data = response.data;
            console.log(data);
            if(data.success=='1'){
                Alert.alert('Edit Nilai Berhasil');
                navigation.goBack();
            }
        });

    }
    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.boxBottom}>
                    <View style={styles.headerboxWhite}>
                        <Text style={styles.judul}>INPUT NILAI MAHASISWA</Text>
                    </View>
                    <View style={styles.boxSummary}>
                        <Image style={{ width: 100, height: 100, marginVertical: 10 }} source={require("../images/profil.png")} />
                        <Text>{params.nama}</Text>
                        <Text style={{ marginBottom: 10 }}>{params.nim}</Text>
                    </View>
                    <View style={styles.boxSummary2}>
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Cource Learning Outcome (CLO)</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text>CLO 4 :</Text>
                        <Text>Mampu berpikir kritis dalam melihat permasalahan tersebut dan memberikan solusi dengan cara membandingkan, mencocokkan, menghubungkan dengan teori-teori dan konsep-konsep yang telah dipelajari di bangku perkuliahan dan menyusunnya sebagai laporan kegiatan dan mempresentasikannya.</Text>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Aspek Penilaian 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Aspek Penilaian 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text>Format dan tata tulis Buku Laporan KP sesuai dengan Buku Panduan KP.</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                                <Text>Materi pembahasan dalam Buku Laporan KP sesuai dengan kegiatan yang dilaksanakan di tempat KP.</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Indikator 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Indikator 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text>1. Format penulisan buku KP sesuai dengan Buku Panduan KP. {"\n"}
                                2. Penulisan kutipan, judul gambar, judul tabel dan format lainnya sesuai.{"\n"}
                                3. Penggunaan Bahasa Indonesia yang baik dan benar.{"\n"}
                                4. Pengetikan/ejaan minim dari kesalahan.</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                                <Text>Materi pembahasan dalam Buku Laporan KP sesuai dengan Catatan Kegiatan KP (Logbook 2).</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Kriteria 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Kriteria 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Kurang</Text>
                                    <Text>0-10</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Cukup</Text>
                                    <Text>11-20</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Baik</Text>
                                    <Text>21-30</Text>
                                </View>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Kurang</Text>
                                    <Text>0-20</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Cukup</Text>
                                    <Text>21-30</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Baik</Text>
                                    <Text>31-40</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Nilai 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Nilai 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TextInput onChangeText={(text) => setclo_4a_akd(text)} placeholder='...' keyboardType='numeric' placeholderTextColor='grey' style={{ borderWidth: 1, textAlign: 'center',color:'black' }} />
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                                <TextInput onChangeText={(text) => setclo_4b_akd(text)} placeholder='...' keyboardType='numeric' placeholderTextColor='grey' style={{ borderWidth: 1, textAlign: 'center',color:'black' }} />
                            </View>
                        </View>
                    </View>

                    <View style={styles.boxSummary2}>
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Cource Learning Outcome (CLO)</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text>CLO 1 :</Text>
                        <Text>Mempelajari kultur budaya kerja di tempat KP dan menerapkan akhlak, kejujuran, kepribadian dan rasa tanggung jawab yang baik.</Text>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Aspek Penilaian</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ alignSelf: 'center' }}>Kepatuhan dan disiplin dalam melaksanakan Konsultasi KP.</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Indikator</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text>1. Melaksanakan konsultasi dengan Pembimbing Akademik sesuai jadwal minimal 3 kali.{"\n"}
                                2. Menyelesaikan revisi/perbaikan Buku Laporan KP sesuai catatan Pembimbing Akademik dengan tepat waktu.</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Kriteria</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Kurang</Text>
                                    <Text>0-10</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Cukup</Text>
                                    <Text>11-20</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Baik</Text>
                                    <Text>21-30</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Nilai</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TextInput onChangeText={(text) => setclo_1_akd(text)} placeholder='...' keyboardType='numeric' placeholderTextColor='grey' style={{ borderWidth: 1, textAlign: 'center', color:'black' }} />
                            </View>
                            <View />
                        </View>
                    </View>


                    <View style={styles.btnSubmit}>
                        <Button title={"Submit"} onPress={handleSubmit} color="#24305A" />
                    </View>
                </View>
                <View style={{ paddingHorizontal: '5%', paddingVertical: '2%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                    <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                    <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
                </View>


            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({

    judul: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#24305A',
        marginBottom: '5%',
        borderBottomWidth: 2,
        paddingBottom: 5,
    },
    boxBottom: {
        padding: 16,
        backgroundColor: '#fff',
        borderRadius: 10,
        width: '94%',
        margin: '5%',
        alignSelf: 'center',

    },
    headerboxWhite: {
        alignItems: 'center'
    },
    boxSummary: {
        borderWidth: 1,
        borderRadius: 30,
        borderColor: 'black',
        marginHorizontal: '15%',
        marginTop: '5%',
        alignItems: 'center'
    },
    boxSummary2: {
        borderWidth: 1,
        borderColor: 'black',
        marginTop: '10%',
        padding: '2%'
    },
    btnSubmit: {
        flex: 1,
        alignItems: "center",
        marginVertical: '3%'


    }
});

export default App;
