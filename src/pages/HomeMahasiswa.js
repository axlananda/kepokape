import React, { useContext, useEffect } from 'react';
import { DrawerItem, DrawerItemList, createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer';
import { Button, View, Text, Image, SafeAreaView } from 'react-native';
import Dashboard from './DashboardMahasiswa';
import Informasi from './InformasiMahasiswa';
import KerjaPraktek from './KerjaPraktekMahasiswa';
import Nilai from './NilaiMahasiswa';
// Connect Variabel Global
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'

function DashboardScreen(props) {
    return (
        <Dashboard {...props} />
    );
}
function InformasiScreen(props) {
    return (
        <Informasi {...props} />
    );
}

function KerjaPraktekScreen(props) {
    return (
        <KerjaPraktek {...props} />
    );
} function NilaiScreen(props) {
    return (
        <Nilai {...props} />
    );
}
const Drawer = createDrawerNavigator();

const App = (props) => {
    const {dataUser } = props;
    
    return (
        <Drawer.Navigator initialRouteName="Dashboard"
            screenOptions={{
                headerShown: true,
                headerStyle: {
                    backgroundColor: '#24305A',
                    elevation: 0, //for android
                    shadowOpacity: 0, //for ios
                    borderBottomWidth: 0, //for ios
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    opacity: 0
                }
            }}
            drawerContentOptions={{
                activeTintColor: 'white',
                activeBackgroundColor: '#24305A',
                inactiveTintColor: '#A8AAB0',
                inactiveBackgroundColor: 'transparent',
                labelStyle: {
                    marginLeft: 25,
                },
            }}
            /* drawerContent={(props) => <View>
                 <View style={{alignItems:'center',paddingVertical:40}}>
                     <Text style={{color:'#24305A', fontWeight:'bold'}}>LAYANAN ADMINISTRASI</Text>
                     <Text style={{color:'#24305A', fontWeight:'bold'}}>AKADEMIK</Text>
                     <View style={{borderBottomWidth:1,width:'70%',marginTop:20}}/>
                 <Image style={{width:100,height:100,marginVertical:20}}source={require("../images/profil.png")}/>
                 <Text style={{fontWeight:'bold',color:'#02164B'}}>Administrator</Text>
                 <Text style={{color:'#A8AAB0'}}>ADMIN LAA</Text>
                 </View>
                 <Text style={{fontWeight:'bold',color:'#24305A',marginLeft:15,marginBottom:10}}>NAVIGATION</Text>
                 
             <DrawerItemList  {...props} />
             </View>}*/
            drawerContent={(props) => <CustomSidebar {...props} dataUser={dataUser} />}
        >
            <Drawer.Screen name="Dashboard" options={{
                drawerLabel: 'Dashboard',
                groupName: 'NAVIGATION',
                activeTintColor: 'white',
                activeBackgroundColor: '#24305A',
                inactiveTintColor: '#A8AAB0',
                inactiveBackgroundColor: 'transparent',
            }} component={DashboardScreen} />
            <Drawer.Screen name="KerjaPraktek" options={{
                drawerLabel: 'Kerja Praktek',
                groupName: 'NAVIGATION',
                activeTintColor: 'white',
                activeBackgroundColor: '#24305A',
                inactiveTintColor: '#A8AAB0',
                inactiveBackgroundColor: 'transparent',
            }} component={KerjaPraktekScreen} />
            <Drawer.Screen name="Informasi" options={{
                drawerLabel: 'Informasi',
                groupName: 'NAVIGATION',
                activeTintColor: 'white',
                activeBackgroundColor: '#24305A',
                inactiveTintColor: '#A8AAB0',
                inactiveBackgroundColor: 'transparent',
            }} component={InformasiScreen} />
            <Drawer.Screen name="Nilai" options={{
                drawerLabel: 'Lihat Nilai',
                groupName: 'NAVIGATION',
                activeTintColor: 'white',
                activeBackgroundColor: '#24305A',
                inactiveTintColor: '#A8AAB0',
                inactiveBackgroundColor: 'transparent',
            }} component={NilaiScreen} />
        </Drawer.Navigator>
    );
}

// Connect Variable Global
const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});

export default connect(
    reduxState
)(App);

const styles = ({
    MainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },

    sectionView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 12,
    },
    separatorLine: {
        flex: 1,
        backgroundColor: 'black',
        height: 1.2,
        marginLeft: 12,
        marginRight: 24,
    },

});



const CustomSidebar = (props) => {
    const { state, descriptors, navigation,dataUser } = props;
    let lastGroupName = '';
    let newGroup = true;

    const logout = () => {
        AsyncStorage.removeItem("user");
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
      }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <DrawerContentScrollView {...props}>
                <View>
                    <View style={{ alignItems: 'center', paddingTop: 40, paddingBottom: 30 }}>
                        <Text style={{ color: '#24305A', fontWeight: 'bold' }}>MAHASISWA</Text>
                        <View style={{ borderBottomWidth: 1, width: '70%', marginTop: 20 }} />
                        <Image style={{ width: 100, height: 100, marginVertical: 20 }} source={require("../images/profil.png")} />
                        <Text style={{ fontWeight: 'bold', color: '#02164B' }}>{dataUser.nim_mahasiswa}</Text>
                        <Text style={{ color: '#A8AAB0', marginBottom: 20 }}>{dataUser.nama_lengkap_mahasiswa}</Text>
                        <Button title="Log Out" onPress={() =>logout() } />
                    </View>
                </View>
                {state.routes.map((route) => {
                    const {
                        drawerLabel,
                        activeTintColor,
                        inactiveTintColor,
                        activeBackgroundColor,
                        inactiveBackgroundColor,
                        groupName
                    } = descriptors[route.key].options;
                    if (lastGroupName !== groupName) {
                        newGroup = true;
                        lastGroupName = groupName;
                    } else newGroup = false;
                    return (
                        <>
                            {newGroup ? (
                                <View style={styles.sectionView}>
                                    <Text key={groupName} style={{ fontWeight: 'bold', color: '#24305A', marginLeft: 15, marginBottom: 10 }}>
                                        {groupName}
                                    </Text>
                                </View>
                            ) : null}
                            <DrawerItem
                                key={route.key}
                                label={
                                    ({ color }) =>
                                        <Text style={{ color, marginLeft: 25 }}>
                                            {drawerLabel}
                                        </Text>
                                }
                                focused={
                                    state.routes.findIndex(
                                        (e) => e.name === route.name
                                    ) === state.index
                                }
                                activeTintColor={activeTintColor}
                                inactiveTintColor={inactiveTintColor}
                                activeBackgroundColor={activeBackgroundColor}
                                inactiveBackgroundColor={inactiveBackgroundColor}
                                onPress={() => navigation.navigate(route.name)}
                            />
                        </>
                    );
                })}
            </DrawerContentScrollView>
        </SafeAreaView>
    );
};