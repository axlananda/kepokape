/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, Component } from 'react';

import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  Alert,
  ImageBackground
} from 'react-native';
import axios from 'axios';
import { saveUserValue } from '../redux/reduxAction';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'
import { config } from '../config';

const App = (props) => {
  const { navigation, saveUserDataAction } = props;
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const handleSignIn = () => {
    axios({
      method: 'post',
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      url: config.system.url+'/index.php/api/login',
      data: 'username=' + username + '&password=' + password
    }).then((response) => {
      var data = response.data;
      console.log('Login : ' + JSON.stringify(data));
      Alert.alert(JSON.stringify(data.message));
     if (data.message == "Selamat Datang Di Aplikasi KepoKape") {

        AsyncStorage.setItem("user", JSON.stringify(data.data));
        saveUserDataAction({ value: data.data });

        if (data.data.user_level == "dosenpemb") {
          navigation.reset({
            index: 0,
            routes: [{ name: 'HomeAkademik' }],
          });
        } else if (data.data.user_level == "laa") {
          navigation.reset({
            index: 0,
            routes: [{ name: 'HomeLAA' }],
          });
        } else if (data.data.user_level == "mahasiswa") {
          navigation.reset({
            index: 0,
            routes: [{ name: 'HomeMahasiswa' }],
          });
        } else if (data.data.user_level == "pemblap") {
          navigation.reset({
            index: 0,
            routes: [{ name: 'HomeLapangan' }],
          });
        } else if (data.data.user_level == "dosenpenguji") {
          navigation.reset({
            index: 0,
            routes: [{ name: 'HomePenguji' }],
          });
        } else {
          Alert.alert("Warning!", JSON.stringify(data.data.message));
        }
        
      }
    });

  }


  return (

    <ImageBackground source={require('../images/backimage.jpeg')} style={styles.background}>

      <SafeAreaView style={styles.wrapper}>

        <View style={styles.page}>
          <ScrollView>
            <View>
              <View style={styles.box}>
                <Image source={require("../images/logo.png")} resizeMode="contain" style={styles.logo} />
                <Text style={styles.textIntro}>KEPO KAPE YUK!</Text>
                <Text style={styles.textlogin}>Aplikasi <Text style={{ fontWeight: 'bold' }}>KEPOKAPE</Text> ini diharapkan
                  {"\n"}dapat mempermudah Administrasi{'\n'}
                  dalam pelaksanaan matakuliah{"\n"} Kerja Praktek</Text>
                <TextInput placeholder="Username" placeholderTextColor="grey" onChangeText={(text) => setUsername(text)} style={styles.input} />
                <TextInput secureTextEntry={true} placeholderTextColor="grey" placeholder="Password" onChangeText={(text) => setPassword(text)} style={styles.input} />
                <Button
                  onPress={() => handleSignIn()}
                  title="Sign In"
                  color="#24305A"

                />
              </View>
            </View>
          </ScrollView>
        </View>

      </SafeAreaView>
    </ImageBackground>

  );
};

const styles = StyleSheet.create({
  // Wrapper
  wrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  // Page
  page: {

    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  input: {
    color: 'black',
    width: '100%',
    marginBottom: '5%',
    fontSize: 16,
    paddingHorizontal: '5%',
    backgroundColor: 'white',
    borderWidth: .5,
    borderRadius: 5
  },
  logo: {
    width: '80%',
    height: 185,
    alignSelf: 'center'
  },

  background: {
    flex: 1,
    alignItems: 'stretch',
    height: '110%',
    resizeMode: 'cover',
  },

  textIntro: {
    color: '#24305A',
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  box: {
    paddingHorizontal: '10%',
    paddingBottom: '2%',
  },
  textlogin: {
    marginVertical: 20,
    textAlign: 'center',
    fontSize: 16,
    color: '#878585',

  }
});
const reduxState = state => ({
  dataUser: state.userDataReducer.userData,
});

const reduxDispatch = (dispatch) => ({
  saveUserDataAction: payload => dispatch(saveUserValue(payload)),
});

export default connect(
  reduxState,
  reduxDispatch,
)(App);

