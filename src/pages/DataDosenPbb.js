import React, { Component, useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    View,
    Image
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, TableWrapper, Row, Rows, Cell } from 'react-native-table-component';
import axios from 'axios';
import { connect } from 'react-redux';
import { config } from '../config';

const App = (props) => {
    const { navigation, dataUser } = props;
    const tableHead = ['No.', 'NIP Dosen', 'Nama Dosen', 'Fakultas', 'Kode Dosen', 'Opsi'];
    const [tableData, setTableData] = useState([
        ['No.', 'NIP Dosen', 'Nama', 'Prodi', 'Kode Dosen', 'Opsi']
    ]);
    const [table, setTable] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/dosen-pemb',
            data: {
                
            }
        }).then((response) => {
            var data = response.data.data;
            var temp = [];
            data.forEach((item,index) => {
                var row = [index+1, item.nip_dosen_pemb, item.nama_dosen_pemb,"FTE", item.kode_dosen_pemb, 
                element(item.nip_dosen_pemb,item.nama_dosen_pemb,item.kode_dosen_pemb)];
                temp.push(row);
            });
            setTableData(temp);
            setTable(true);
        });
    }

    const _alertIndex = (id_dosen_pemb, nip_dosen_pemb, nama_dosen_pemb, kode_dosen_pemb) => {
        Alert.alert(
            'Delete',
            nama_dosen_pemb,
            [
                { text: 'Cancel', onPress: () => { } },
                { text: 'OK', onPress: () => console.log("OK") },
            ],
            { cancelable: false }
        )
    }

    const element = (id_dosen_pemb, nip_dosen_pemb, nama_dosen_pemb, kode_dosen_pemb) => (
        <TouchableOpacity onPress={() => _alertIndex(id_dosen_pemb,nip_dosen_pemb,nama_dosen_pemb, kode_dosen_pemb)}>
            <View style={styles.btn}>
                <Text style={styles.btnText}>Delete</Text>
            </View>
        </TouchableOpacity>
    );
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC', flex: 1 }}>
            <View style={styles.boxHeader}>
                <Text style={styles.boxHeadertxt1}>Dashboard {'>'} Data Dosen Pembimbing</Text>
            </View>
            <View style={styles.boxBottom}>
                <View style={styles.headerboxWhite}>
                    <Text style={{ fontSize:16,color:'#24305A', alignSelf: 'center' }}>Data Dosen Pembimbing</Text>
                    <View style={{marginHorizontal:'10%',marginVertical:10 }}>
                    <Button
                        //onPress={() => handleSignIn()}
                        title="Tambah Dosen Pembimbing"
                        color="#24305A"
                    />
                    </View>
                    <View><TextInput placeholder='Search' placeholderTextColor="grey" style={{ alignSelf:'center',borderWidth: 1, width: '80%', paddingVertical: 0, borderColor: '#A8AAB0' }} /></View>
                </View>

                <ScrollView>
                    <ScrollView horizontal={true}>
                        <Table>
                            <Row data={tableHead} widthArr={[50, 100, 250, 150, 150, 80]} style={styles.head} textStyle={styles.textHead} />

                            {
                                tableData.map((rowData, index) => (
                                    <Row
                                        key={index}
                                        data={rowData}
                                        style={[index % 2 && { backgroundColor: '#EFEEEF' }]}
                                        textStyle={styles.text}
                                        widthArr={[50, 100, 250, 150, 150,80]}
                                    />
                                ))
                            }

                        </Table>

                    </ScrollView>

                </ScrollView>
            </View>
            <View style={{ position: 'relative', paddingHorizontal: '5%', paddingVertical: '2%', marginTop: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    textData: {
        alignSelf: 'center',
        marginBottom: 10,
        color: '#24305A'
    },
    boxHeader: {
        zIndex: -1,
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        marginBottom: '122%'
    },
    boxBottom: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        marginTop: '15%',
        alignSelf: 'center',
        height: '85%'
    },
    head: {
        height: 40,
        backgroundColor: '#24305A',
        alignItems:'center'
    },
    text: {
        margin: 6,
        color: 'black',
        alignSelf: 'center',
        marginBottom: 10
    },
    textHead: {
        color: 'white',
        alignSelf: 'center'
    },
    btn: {
        width: 58, height: 20, backgroundColor: '#24305A', borderRadius: 2,
        alignSelf: 'center'
    },
    btnText: {
        textAlign: 'center', color: 'white'
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 16,
        marginLeft: '5%',
        marginBottom: '30%',
        marginTop: '2%'
    },
    headerboxWhite: {
        marginBottom: 20
    }
});

const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});

export default connect(
    reduxState
)(App);
