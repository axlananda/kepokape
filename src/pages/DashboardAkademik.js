
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, Row, Rows } from 'react-native-table-component';
import axios from 'axios';
import { connect } from 'react-redux';
import { config } from '../config';

const screen = Dimensions.get("window");
const hp = screen.height;
const wp = screen.width;

const App = (props) => {
    const { navigation, dataUser } = props;
    const tableHead = ['NIM', 'Nama'];
    const [tableData, setTableData] = useState();
    const [nilaiDone, setnilaiDone] = useState();
    const [nilaiNone, setnilaiNone] = useState();

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url + '/index.php/api/mahasiswa-all-akademik',
            data: 'id_dosen_pemb=' + dataUser.id_dosen_pemb
        }).then((response) => {
            var data = response.data.data;
            var temp = [];

            data.forEach(item => {
                var row = [item.nim_mahasiswa, item.nama_lengkap_mahasiswa];
                temp.push(row);
            });
            setTableData(temp);

        });

        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url + '/index.php/api/sum-akademik',
            data: 'id_dosen_pemb=' + dataUser.id_dosen_pemb
        }).then((response) => {
            var data1 = response.data.data1;
            var data2 = response.data.data2;

            setnilaiDone(data1)
            setnilaiNone(data2)

        });
    }
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC' }}>
            <ScrollView>
                <View style={styles.boxHeader}>
                    <Text style={styles.boxHeadertxt1}>Selamat Datang di Aplikasi{'\n'}KEPOKAPE</Text>
                    <Text style={styles.boxHeadertxt2}>Aplikasi KEPOKAPE ini diharapkan dapat{'\n'}mempermudah Administrasi dalam pelaksanaan{'\n'}matakuliah Kerja Praktek</Text>
                </View>

                    <View style={styles.boxGreen}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../images/report.png')} style={{ width: wp / 6, height: wp / 6 }} resizeMode="contain" />
                            </View>
                            <View style={{ flex: 2.5 }}>
                                <Text style={styles.boxGreentxt1}>{nilaiDone}</Text>
                                <Text style={styles.boxGreentxt2}>Mahasiswa <Text style={{ fontWeight: 'bold' }}>telah</Text> di nilai</Text>
                            </View>
                        </View>
                    </View>


                <View style={styles.boxRed}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../images/scores.png')} style={{ width: wp / 6, height: wp / 6 }} resizeMode="contain" />
                        </View>
                        <View style={{ flex: 2.5 }}>
                            <Text style={styles.boxRedtxt1}>{nilaiNone}</Text>
                            <Text style={styles.boxRedtxt2}>Mahasiswa <Text style={{ fontWeight: 'bold' }}>belum</Text> di nilai</Text>
                        </View>
                    </View>
                </View>



                <View style={styles.boxWhite}>
                    <View style={styles.headerboxWhite}>
                        <TouchableOpacity onPress={() => getData()}>
                            <Text>Refresh</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ borderBottomWidth: 2, paddingBottom: 5 }}>
                            <Text>Semua Data</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text>Kelas</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.kontenboxWhite}>
                        <Text>Progress Mahasiswa</Text>
                        <Text style={{ marginTop: '2%' }}>Status 0 :</Text>
                        <Progress.Bar color="red" progress={0.7} width={null}></Progress.Bar>
                        <Text>70.00% Not Completed</Text>
                        <Text style={{ marginTop: '2%' }}>Status 1 :</Text>
                        <Progress.Bar color="#9D02A0" progress={0.3} width={null}></Progress.Bar>
                        <Text>30.00% Completed</Text>
                        <Text style={{ marginTop: '2%' }}>Status 2 :</Text>
                        <Progress.Bar color="#109D51" progress={0.2} width={null}></Progress.Bar>
                        <Text>20.00% Completed</Text>
                        <Text style={{ marginTop: '2%' }}>Status 3 :</Text>
                        <Progress.Bar color="#0683DE" progress={0.1} width={null}></Progress.Bar>
                        <Text>10.00% Completed</Text>
                        <Text style={{ marginTop: '2%' }}>Status 4 :</Text>
                        <Progress.Bar color="#EEEFF2" progress={0.0} width={null}></Progress.Bar>
                        <Text>0.00% Completed</Text>

                    </View>
                    <View style={styles.boxSummary}>
                        <View style={styles.headerboxSummary}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.headertextSummary1}>Summary</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: 'white' }}>10</Text>
                                <Text style={{ color: 'white', marginBottom: 10 }}> Mahasiswa</Text>
                            </View>
                        </View>
                        <View style={styles.kontenboxSummary}>

                            <View style={styles.listSummary}>
                                <View>
                                    <Text>Status 0</Text>
                                    <Text style={styles.txtlistSummary1}>Mahasiswa Pasif (Tidak / belum{'\n'}melakukan apapun)</Text>
                                </View>
                                <View>
                                    <Text style={styles.txtlistSummary2}>7</Text>
                                    <Text style={styles.txtlistSummary1}>Mahasiswa</Text>

                                </View>
                            </View>
                            <View style={styles.listSummary}>
                                <View>
                                    <Text>Status 1</Text>
                                    <Text style={styles.txtlistSummary1}>Sudah melakukan penjajakan tempat KP</Text>
                                </View>
                                <View style={{}}>
                                    <Text style={styles.txtlistSummary2}>3</Text>
                                    <Text style={styles.txtlistSummary1}>Mahasiswa</Text>

                                </View>
                            </View>
                            <View style={styles.listSummary}>
                                <View>
                                    <Text>Status 2</Text>
                                    <Text style={styles.txtlistSummary1}>Sudah mengajukan proposal KP dan{'\n'}disetujui Pembimbing Akademik (Dosen Wali)</Text>
                                </View>
                                <View>
                                    <Text style={styles.txtlistSummary2}>2</Text>
                                    <Text style={styles.txtlistSummary1}>Mahasiswa</Text>

                                </View>
                            </View>
                            <View style={styles.listSummary}>
                                <View>
                                    <Text>Status 3</Text>
                                    <Text style={styles.txtlistSummary1}>Sudah terbit surat lamaran KP dari LAA{'\n'}dan diajukan ke Instansi calon tempat KP</Text>
                                </View>
                                <View>
                                    <Text style={styles.txtlistSummary2}>1</Text>
                                    <Text style={styles.txtlistSummary1}>Mahasiswa</Text>

                                </View>
                            </View>
                            <View style={styles.listSummary}>
                                <View>
                                    <Text>Status 4</Text>
                                    <Text style={styles.txtlistSummary1}>Sudah memperoleh surat balasan dari{'\n'}Insansi calon tempat KP</Text>
                                </View>
                                <View>
                                    <Text style={styles.txtlistSummary2}>0</Text>
                                    <Text style={styles.txtlistSummary1}>Mahasiswa</Text>

                                </View>
                            </View>
                        </View>
                    </View>

                    <Text style={{ color: '#24305A', fontSize: 20, alignSelf: 'center', marginVertical: '5%' }}>Data Mahasiswa</Text>

                    <TextInput placeholder='Search' placeholderTextColor="grey" style={{ borderWidth: 1, width: '60%', alignSelf: 'center', paddingVertical: 0, borderColor: '#A8AAB0' }} />

                    <ScrollView horizontal={true} style={{ marginHorizontal: '3%', marginTop: '5%' }}>
                        <View style={{ width: wp * 1 }}>
                            <Table borderStyle={{ borderWidth: 2, borderColor: '#24305A' }}>
                                <Row data={tableHead} style={{ backgroundColor: '#24305A', height: 40 }} textStyle={{ color: 'white', alignSelf: 'center' }} />
                                <Rows data={tableData} textStyle={{ marginLeft: '2%', marginVertical: '2%' }} />
                            </Table>
                        </View>
                    </ScrollView>

                </View>

                <View style={{ paddingHorizontal: '5%', paddingVertical: '2%', marginBottom: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                    <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                    <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
                </View>


            </ScrollView>

        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    boxHeader: {
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        alignItems: 'center',

    },
    boxGreen: {
        marginHorizontal: '3%',
        paddingVertical: '5%',
        backgroundColor: '#72CA95',
        marginTop: '5%',
        borderRadius: 10,
    },
    boxRed: {
        marginHorizontal: '3%',
        paddingVertical: '5%',
        backgroundColor: '#FF0000',
        marginTop: '5%',
        borderRadius: 10,
    },
    boxGreentxt1: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },
    boxGreentxt2: {
        fontSize: 16,
        color: '#FFFFFF',

    },
    boxRedtxt1: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },
    boxRedtxt2: {
        fontSize: 16,
        color: '#FFFFFF',

    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 20,
        textAlign: 'center',

    },
    boxHeadertxt2: {
        color: '#FFFFFF',
        fontSize: 14,
        textAlign: 'center',
        marginTop: '3%',
    },
    boxWhite: {
        backgroundColor: '#FFFFFF',
        marginHorizontal: '3%',
        marginTop: '5%',
        paddingVertical: '4%',
        borderRadius: 10,
        marginBottom: '5%',
    },
    headerboxWhite: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    kontenboxWhite: {
        paddingHorizontal: '4%',
        marginTop: '6%',
    },
    boxSummary: {
        borderWidth: 1,
        borderColor: '#24305A',
        marginHorizontal: '3%',
        marginTop: '5%',
    },
    headerboxSummary: {
        backgroundColor: '#24305A',
        flexDirection: 'row',
        justifyContent: 'space-around',
        color: '#FFFFFF',
        paddingVertical: '2%'
    },
    kontenboxSummary: {
        paddingHorizontal: '5%',
        paddingVertical: '3%',

    },
    listSummary: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: '2%'
    },
    headertextSummary1: {
        color: 'white',
        fontSize: 18
    },
    txtlistSummary1: {
        fontSize: 11,
        color: '#A8AAB0',
    },
    txtlistSummary2: {
        fontSize: 18,
        color: '#A8AAB0',
        textAlign: 'right'
    }
});

const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});

export default connect(
    reduxState
)(App);
