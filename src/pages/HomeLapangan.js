import * as React from 'react';
import { Button, View,Text,Image } from 'react-native';
import { DrawerItems,DrawerItemList, createDrawerNavigator } from '@react-navigation/drawer';
import DashboardLapangan from './DashboardLapangan';
import NilaiLapangan from './NilaiLapangan';
import LihatNilaiLapangan from './LihatNilaiLapangan';
import HasilInputNilaiLapangan from './HasilInputNilaiLapangan';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'

function DashboardLapanganScreen(props) {
    return (
        <DashboardLapangan {...props} />
    );
}
function NilaiLapanganScreen(props) {
    return (
        <NilaiLapangan {...props} />
    );
}
function LihatNilaiLapanganScreen(props) {
    return (
        <LihatNilaiLapangan {...props} />
    );
}
function HasilInputNilaiLapanganScreen(props) {
    return (
        <HasilInputNilaiLapangan {...props} />
    );
}

const Drawer = createDrawerNavigator();

const App = (props) => {
    const {navigation,dataUser}=props;
    const logout = () => {
        AsyncStorage.removeItem("user");
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
      }
    
    return (
        <Drawer.Navigator initialRouteName="Dashboard"
            screenOptions={{
                headerShown: true,
                headerStyle: {
                    backgroundColor: '#24305A',
                    elevation: 0, //for android
                    shadowOpacity: 0, //for ios
                    borderBottomWidth: 0, //for ios
                },
                headerTintColor: 'white',
                headerTitleStyle: {
                    opacity: 0
                }
            }}
            drawerContentOptions={{
                activeTintColor: 'white',
                activeBackgroundColor: '#24305A',
                inactiveTintColor: '#A8AAB0',
                inactiveBackgroundColor: 'transparent',
                labelStyle: {
                    marginLeft: 25,
                  },
            }}
            
            drawerContent={(props) => <View>
                <View style={{alignItems:'center',paddingTop:40,paddingBottom:30}}>
                    <Text style={{color:'#24305A', fontWeight:'bold'}}>PEMBIMBING LAPANGAN</Text>
                    <View style={{borderBottomWidth:1,width:'70%',marginTop:20}}/>
                <Image style={{width:100,height:100,marginVertical:20}}source={require("../images/profil.png")}/>
                <Text style={{fontWeight:'bold',color:'#02164B'}}>{dataUser.nip_pbb_lapangan}</Text>
                <Text style={{color:'#A8AAB0',marginBottom:20}}>{dataUser.nama_pbb_lapangan}</Text>
                <Button title="Log Out" onPress={()=>logout()}/>
                </View>
                <Text style={{fontWeight:'bold',color:'#24305A',marginLeft:15,marginBottom:10}}>NAVIGATION</Text>
                
            <DrawerItemList  {...props} />
            </View>}
            >   
            <Drawer.Screen name="Dashboard" component={DashboardLapanganScreen} />
            <Drawer.Screen name="Data Mahasiswa" component={NilaiLapanganScreen} />
            <Drawer.Screen name="Lihat Hasil Input" component={LihatNilaiLapanganScreen} />
        </Drawer.Navigator>
    );
}

// Connect Variable Global
const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});

export default connect(
    reduxState
)(App);
