import React, { Component, useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    View,
    Image
} from 'react-native';
import * as Progress from 'react-native-progress';
import axios from 'axios';
import { useRoute } from "@react-navigation/native";
import { config } from '../config';

const App = (props) => {
    const { navigation } = props;
    const { params } = useRoute();
    const [clo_2_upkp, setclo_2_upkp] = useState();
    const [clo_4_upkp, setclo_4_upkp] = useState();


    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/detail-nilai-upkp',
            
            data: 'nim=' + params.nim
           
        }).then((response) => {
            var data = response.data.data;
            
            setclo_2_upkp(data.clo_2_upkp);
            setclo_4_upkp(data.clo_4_upkp);
        });
    }

    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC', flex: 1 }}>
            <View style={styles.boxBottom}>
                <View style={styles.headerboxWhite}>
                    <Text style={styles.textData}>LIHAT HASIL INPUT NILAI</Text>
                </View>
                <View style={styles.boxProfil}>
                        <Image style={{ width: 100, height: 100, marginVertical: 10 }} source={require("../images/profil.png")} />
                        <Text style={{textAlign:'center'}}>{params.nama}</Text>
                        <Text style={{ marginBottom: 10 }}>{params.nim}</Text>
                    </View>
                <View>
                <View style={{ backgroundColor: '#24305A', alignItems: 'center', paddingVertical: 5 }}>
                            <Text style={{ color: 'white' }}>
                                Course Learning Outcome (CLO 2)
                        </Text>
                        </View>
                        <View style={{ alignItems: 'center', paddingVertical: 5, borderWidth: 1 }}>
                            <Text style={{ color: '#24305A' }}>
                                {clo_2_upkp}
                            </Text>
                        </View>
                </View>

                <View style={{ marginTop: 20 }}>
                    <View style={{ backgroundColor: '#24305A', alignItems: 'center', paddingVertical: 5 }}>
                        <Text style={{ color: 'white' }}>
                            Course Learning Outcome (CLO 4)
                        </Text>
                    </View>
                    <View style={{ alignItems: 'center', paddingVertical: 5, borderWidth: 1 }}>
                        <Text style={{ color: '#24305A' }}>
                            {clo_4_upkp}
                        </Text>
                    </View>
                </View>

            </View>
            <View style={{ position: 'absolute',bottom:0, paddingHorizontal: '5%', paddingVertical: '2%', marginTop: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    textData: {
        color: '#24305A',
        fontSize: 16,
        borderBottomWidth: 2
    },
    boxHeader: {
        zIndex: -1,
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        marginBottom: '122%'
    },
    boxBottom: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        margin: '5%',
        alignSelf: 'center',
        height: '90%'
    },
    head: {
        height: 40,
        justifyContent: 'space-between',
        backgroundColor: '#24305A'
    },
    text: {
        margin: 10,
        color: 'black',
        alignSelf: 'center',
        marginBottom: 10,
    },
    textHead: {
        color: 'white',
        alignSelf: 'center',

    },
    btn: {
        width: 58, height: 20, backgroundColor: '#24305A', borderRadius: 2,
        alignSelf: 'center'
    },
    btnText: {
        textAlign: 'center', color: 'white'
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 16,
        marginLeft: '5%',
        marginBottom: '30%',
        marginTop: '2%'
    },
    headerboxWhite: {
        alignItems: 'center',
        marginBottom: 20
    },
    boxProfil: {
        borderWidth: 1,
        borderRadius: 30,
        borderColor: 'black',
        marginHorizontal: '15%',
        marginTop: '5%',
        alignItems: 'center',
        marginBottom: '10%'
    },
});

export default App;