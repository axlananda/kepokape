
import React from 'react';

import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button
} from 'react-native';

const App = (props) => {
    const {navigation}=props;
    return (
        <SafeAreaView >
            <Text style={{fontSize:28,textAlign:'center',marginVertical:'50%'}}>KERJA PRAKTEK SCREEN</Text>
            <Text style={{textAlign:'center',fontSize:16}}>Aplikasi masih dalam proses pengembangan</Text>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({

});

export default App;
