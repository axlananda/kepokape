/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState,useEffect, Component } from 'react';

import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Button,
  Alert,
  ImageBackground
} from 'react-native';
import axios from 'axios';
import { saveUserValue } from '../redux/reduxAction';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage'

const App = (props) => {
  const { navigation,saveUserDataAction } = props;
  
  
  useEffect(() => {
    checkLogin();
  }, []);

  const checkLogin = async () => {
    var user = await AsyncStorage.getItem('user');
    let data = JSON.parse(user);
    saveUserDataAction({ value: data });

    if (data != null) {
      setTimeout(() => {
        var page;
        if (data.user_level == "mahasiswa") {
          page = 'HomeMahasiswa';
        } else if (data.user_level == "pemblap") {
          page = 'HomeLapangan';
        }else if (data.user_level == "dosenpemb") {
          page = 'HomeAkademik';
        }else if (data.user_level == "dosenpenguji") {
          page = 'HomePenguji';
        }else if (data.user_level == "laa") {
          page = 'HomeLAA';
        }
        navigation.reset({
          index: 0,
          routes: [{ name: page }],
        });

      }, 2000);
    } else {
      setTimeout(() => {
        navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        });
      }, 2000);
    }
  };

  return (
    
    <ImageBackground source={require('../images/backimage.jpeg')} style={styles.background}>
      
      <SafeAreaView style={styles.wrapper}>
      
        <View style={styles.page}>
        <ScrollView>
          <View>
            <View style={styles.box}>
              <Image source={require("../images/logo.png")} resizeMode="contain" style={styles.logo} />
              <Text style={styles.textIntro}>KEPO KAPE YUK!</Text>

            </View>
          </View>
          </ScrollView>
        </View>
        
      </SafeAreaView>
    </ImageBackground>
    
  );
};

const styles = StyleSheet.create({
  // Wrapper
  wrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  // Page
  page: {
    
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  input: {

    width: '100%',
    marginBottom: '5%',
    fontSize: 16,
    paddingHorizontal: '5%',
    backgroundColor: 'white',
    borderWidth:.5,
    borderRadius:5
  },
  logo: {
    width: '80%',
    height: 185,
    alignSelf: 'center'
  },

  background: {
    flex: 1,
    alignItems: 'stretch',
    height:'110%',
    resizeMode: 'cover',
  },

  textIntro: {
    color: '#24305A',
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  box: {
    paddingHorizontal: '10%',
    paddingBottom: '2%',
  },
  textlogin: {
    marginVertical: 20,
    textAlign: 'center',
    fontSize: 16,
    color: '#878585',

  }
});
const reduxState = state => ({
  dataUser: state.userDataReducer.userData,
});

const reduxDispatch = (dispatch) => ({
  saveUserDataAction: payload => dispatch(saveUserValue(payload)),
});

export default connect(
  reduxState,
  reduxDispatch,
)(App);

