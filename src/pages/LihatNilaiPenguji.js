import React, { Component, useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    View,
    Image
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, TableWrapper, Row, Rows, Cell } from 'react-native-table-component';
import { useRoute } from "@react-navigation/native";
import axios from 'axios';
import { connect } from 'react-redux';
import { config } from '../config';

const App = (props) => {
    const { params} = useRoute();
    const { navigation, dataUser } = props;
    const tableHead = ['No.', 'Nama', 'Nim', 'Prodi', 'Opsi'];
    const [tableData, setTableData] = useState([
        ['No.', 'Nama', 'NIM', 'Prodi', 'Opsi']
    ]);
    const [table, setTable] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/lihat-nilai-penguji',
            data: 'id_dosen_penguji=' + dataUser.id_dosen_penguji

        }).then((response) => {
            var data = response.data.data;
            var temp = [];
            data.forEach((item,index) => {
                var row = [index+1, item.nama_lengkap_mahasiswa, item.nim_mahasiswa, "Teknik Komputer", element(item.nim_mahasiswa, item.nama_lengkap_mahasiswa)];
                temp.push(row);
            });
            setTableData(temp);
            setTable(true);
        });
    }

    const _alertIndex = (nim_mahasiswa, nama_lengkap_mahasiswa) => {
        Alert.alert(
            'Lihat Nilai',
            nama_lengkap_mahasiswa,
            [
                { text: 'Cancel', onPress: () => { } },
                { text: 'OK', onPress: () => navigation.navigate("HasilInputNilaiPenguji", {nama:nama_lengkap_mahasiswa,nim:nim_mahasiswa}) },
            ],
            { cancelable: false }
        );
    }

    const element = (nim_mahasiswa, nama_lengkap_mahasiswa) => (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: '5%', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => _alertIndex(nim_mahasiswa, nama_lengkap_mahasiswa)}>
                <View style={styles.btnView}>
                    <Text style={styles.btnText}>View</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => _alertIndex2(nim_mahasiswa, nama_lengkap_mahasiswa)}>
                <View style={styles.btnView}>
                    <Text style={styles.btnText}>Edit</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => _alertIndex3(nim_mahasiswa, nama_lengkap_mahasiswa)}>
                <View style={styles.btnView}>
                    <Text style={styles.btnText}>Delete</Text>
                </View>
            </TouchableOpacity>
        </View>

    );

    const _alertIndex2 = (nim_mahasiswa, nama_lengkap_mahasiswa) => {
        Alert.alert(
            'Edit Nilai',
            nama_lengkap_mahasiswa,
            [
                { text: 'Cancel', onPress: () => { } },
                { text: 'OK', onPress: () => navigation.navigate('EditNilaiPenguji', { nim: nim_mahasiswa, nama: nama_lengkap_mahasiswa }) },
            ],
            { cancelable: false }
        )
    }

    const _alertIndex3 = (nim_mahasiswa, nama_lengkap_mahasiswa) => {
        Alert.alert(
            'Delete Nilai',
            nama_lengkap_mahasiswa,
            [
                { text: 'Cancel', onPress: () => { } },
                { text: 'OK', onPress: () => delete_nilai(nim_mahasiswa) },
            ],
            { cancelable: false }
        )
    };

    const delete_nilai = (nim) => {
        axios({
            method: 'post',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            url: config.system.url+'/index.php/api/delete-nilai-upkp',
            data: 'nim=' + nim
        }).then((response) => {
            var data = response.data;
            console.log(data);
            if(data.success=='1'){
                Alert.alert('Delete Nilai Berhasil');
                getData();
            }
        });

    }
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC', flex: 1 }}>
            <View style={styles.boxHeader}>
                <Text style={styles.boxHeadertxt1}>Dashboard {'>'} Lihat Hasil Input</Text>
            </View>
            <View style={styles.boxBottom}>
                <View style={styles.headerboxWhite}>
                    <View style={{ fontWeight: 'bold', alignSelf: 'center', marginBottom: 20 }}><Text>LIHAT HASIL INPUT NILAI MAHASISWA</Text></View>
                </View>

                <ScrollView>
                    <ScrollView horizontal={true}>
                        <Table>
                            <Row data={tableHead} widthArr={[50, 300, 150, 150, 250]} style={styles.head} textStyle={styles.textHead} />

                            {
                                tableData.map((rowData, index) => (
                                    <Row
                                        key={index}
                                        data={rowData}
                                        style={[index % 2 && { backgroundColor: '#EFEEEF' }]}
                                        textStyle={styles.text}
                                        widthArr={[50, 300, 150, 150, 250]}
                                    />
                                ))
                            }

                        </Table>

                    </ScrollView>

                </ScrollView>
                <TouchableOpacity onPress={()=>getData()}>
                    <View style={{backgroundColor:'#24305A',alignItems:'center',paddingVertical:10}}>
                        <Text  style={{color:'white'}}>Refresh</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{position:'absolute',width:'100%',bottom:0,paddingHorizontal:'5%',paddingVertical:'2%',flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
               <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({

    boxHeader: {
        zIndex: -1,
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        marginBottom: '122%'
    },
    boxBottom: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        marginTop: '15%',
        alignSelf: 'center',
        height: '85%'
    },
    head: {
        height: 40,
        backgroundColor: '#24305A',
    },
    text: {
        margin: 6,
        color: 'black',
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginBottom: 10
    },
    textHead: {
        color: 'white',
        alignSelf: 'center'
    },

    btnView: {
        width: 58,
        height: 20,
        backgroundColor: '#24305A',
        borderRadius: 2,
        alignSelf: 'center',
    },

    btnText: {
        textAlign: 'center', color: 'white'
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 16,
        marginLeft: '5%',
        marginBottom: '30%',
        marginTop: '2%'
    }



});

const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});

export default connect(
    reduxState
)(App);