
import React, { useEffect, useState, Component } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Dimensions,
    TouchableOpacity,
    useWindowDimensions,
    FlatList,
    LogBox
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, Row, Rows } from 'react-native-table-component';
import axios from 'axios';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { config } from '../config';
LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

const screen = Dimensions.get("window");
const hp = screen.height;
const wp = screen.width;

const App = (props) => {
    const { navigation } = props;
    const tableHead = ['No.', 'Nama Perusahaan', 'Alamat Perusahaan', 'Kontak Perusahaan'];
    const [tableData, setTableData] = useState([
        ['No.', 'Nama Perusahaan', 'Alamat Perusahaan', 'Kontak Perusahaan']
    ]);
    const [table, setTable] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/perusahaan-rekruitasi',
            data: {

            }
        }).then((response) => {
            var data = response.data.data;
            var temp = [];
            data.forEach((item, index) => {
                var row = [index+1, item.nama_perusahaan, item.alamat_lkp, item.kontak_perusahaan];
                temp.push(row);
            });
            setTableData(temp);
            setTable(true);
        });
    }

    

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: '#1' },
        { key: 'second', title: '#2' },
        { key: 'third', title: '#3' },
    ]);

    const dataTimeline = [
        { id: "1", waktu: "8 Nov 2019 \n sd. 6 Des 2019", judul: "Penjajakan Lokasi Kerja Praktik", deskripsi: "Mahasiswa melaksanakan Penjajakan Lokasi Kerja Praktek dengan membawa Form Berita Acara Penjajakan" },
        { id: "2", waktu: "8 Nov 2019 \n sd. 3 Jan 2020", judul: "Lapor Hasil Penjajakan", deskripsi: "Mahasiswa melaporkan Hasil Penjajakan Lokasi KP dan melakukan konsultasi pembuatan Proposal KP dengan Dosen Wali" },
        { id: "3", waktu: "8 Nov 2019 \n sd. 3 Jan 2020", judul: "Approval Form KP Oleh Dosen Wali", deskripsi: "Dosen Wali menandatangani Berita Acara Penjajakan Lokasi KP, Proposal KP dan Form Surat Permohonan KP" },
        { id: "4", waktu: "6 Jan 2020 \n sd. 17 Jan 2020", judul: "Pengajuan Surat Permohonan KP Ke LAA", deskripsi: "Mahasiswa mengajukan permohonan pembuatan Surat Permohonan KP ke LAA, dilampiri Form Surat Permohonan KP, Berita Acara Survei Lokasi KP dan Proposal KP" },
        { id: "5", waktu: "6 Jan 2020 \n sd. 17 Jan 2020", judul: "Menyerahkan Surat Permohonan KP ke Instansi", deskripsi: "Mahasiswa menyerahkan Surat Permohonan KP ke Instansi yang dituju dilampiri Proposal KP" }

    ]

    const dataTimeline2 = [
        { id: "1", waktu: "15 Feb 2020 \n sd. 25 Feb 2020", judul: "Balasan proposal yang di terima oleh LAA", deskripsi: "FTE menerima surat Balasan Permohonan KP diterima/ditolak dari Instansi" },
        { id: "2", waktu: "20 Maret 2020", judul: "Lapor Hasil Penjajakan", deskripsi: "Mahasiswa melaporkan Hasil Penjajakan Lokasi KP dan melakukan konsultasi pembuatan Proposal KP dengan Dosen Wali" },
        { id: "3", waktu: "30 Apr 2020", judul: "Pembekalan Kerja Praktek", deskripsi: "Dosen Wali/Pembimbing Akademik memberikan Pembekalan KP kepada mahasiswa" },
        { id: "4", waktu: "2 Jun 2020 \n sd. 10 Jul 2020", judul: "Pelaksanaan Kerja Praktek", deskripsi: "Pelaksanaan KP, pembimbingan lapangan/akademik dan pembuatan Laporan KP" },
        { id: "5", waktu: "13 Jul 2020 \n sd. 17 Jul 2020", judul: "Konsultasi Dengan Dosen Pembimbing Akademik", deskripsi: "Mahasiswa melakukan konsultasi Laporan KP dengan Pembimbing Akademik" }

    ]
    const dataTimeline3 = [
        { id: "1", waktu: "17 Jul 2020 \n sd. 19 Jul 2020", judul: "Unggah Laporan", deskripsi: "Mahasiswa unggah bab 3 dan 4 Laporan KP, Pembimbing Akademik menilai mhs bimbingan" },
        { id: "2", waktu: "20 Jul 2020 \n sd. 24 Jul 2020", judul: "Pelaksanaan UPKP", deskripsi: "Mahasiswa melaksanakan UPKP, Dosen Penguji KP memberi penilaian" },
        { id: "3", waktu: "20 Jul 2020 \n sd. 24 Jul 2020", judul: "Similarity Laporan KP Oleh LAA", deskripsi: "LAA memeriksa similarity laporan KP" },
        { id: "4", waktu: "24 Jul 2020 \n sd. 30 Jul 2020", judul: "Kompilasi Nilai", deskripsi: "-Dosen kompilasi nilai \n -Mhs jilid laporan KP versi lengkap \n -Mhs unggah Bab 1 s/d DaPus ke openlibrary" },
        { id: "5", waktu: "13 Jul 2020 \n sd. 17 Jul 2020", judul: "Penutupan Kerja Praktek", deskripsi: "Ucapan terima kasih ke Instansi oleh LAA" }

    ]

    const FirstRoute = () => (
        <View style={{ flex: 1, backgroundColor: 'white', height: 100 }}>
            <FlatList
                data={dataTimeline}
                keyExtractor={(item) => {
                    return item.id;
                }}
                style={{ paddingHorizontal: '2%' }}
                renderItem={({ item, index }) => {
                    return (
                        <View style={{ flexDirection: 'row', marginBottom: '3%' }}>
                            <View style={{ flex: 1, paddingRight: 8 }}>
                                <Text style={{ textAlign: 'center', marginTop: 15, fontSize: 12, color: '#A8AAB0' }}>{item.waktu}</Text>
                                <View style={{ marginTop: '5%', flex: 1, borderLeftWidth: 1.5, alignSelf: 'center', borderLeftColor: '#A8AAB0' }} />
                            </View>
                            <View style={{ flex: 2, backgroundColor: '#EEEFF2', borderRadius: 10, padding: 10, marginTop: 10 }}>
                                <Text style={{ marginBottom: 10, fontWeight: 'bold', color: '#28B0FD' }}>{item.judul}</Text>
                                <Text style={{ color: '#A8AAB0' }}>{item.deskripsi}</Text>
                            </View>
                        </View>
                    );
                }}
            />
        </View>
    );

    const SecondRoute = () => (
        <View style={{ flex: 1, backgroundColor: 'white', height: 100 }}>
            <FlatList
                data={dataTimeline2}
                keyExtractor={(item) => {
                    return item.id;
                }}
                style={{ paddingHorizontal: '2%' }}
                renderItem={({ item, index }) => {
                    return (
                        <View style={{ flexDirection: 'row', marginBottom: '3%' }}>
                            <View style={{ flex: 1, paddingRight: 8 }}>
                                <Text style={{ textAlign: 'center', marginTop: 15, fontSize: 12, color: '#A8AAB0' }}>{item.waktu}</Text>
                                <View style={{ marginTop: '5%', flex: 1, borderLeftWidth: 1.5, alignSelf: 'center', borderLeftColor: '#A8AAB0' }} />
                            </View>
                            <View style={{ flex: 2, backgroundColor: '#EEEFF2', borderRadius: 10, padding: 10, marginTop: 10 }}>
                                <Text style={{ marginBottom: 10, fontWeight: 'bold', color: '#28B0FD' }}>{item.judul}</Text>
                                <Text style={{ color: '#A8AAB0' }}>{item.deskripsi}</Text>
                            </View>
                        </View>
                    );
                }}
            />
        </View>
    );

    const ThirdRoute = () => (
        <View style={{ flex: 1, backgroundColor: 'white', height: 100 }}>
            <FlatList
                data={dataTimeline3}
                keyExtractor={(item) => {
                    return item.id;
                }}
                style={{ paddingHorizontal: '2%' }}
                renderItem={({ item, index }) => {
                    return (
                        <View style={{ flexDirection: 'row', marginBottom: '3%' }}>
                            <View style={{ flex: 1, paddingRight: 8 }}>
                                <Text style={{ textAlign: 'center', marginTop: 15, fontSize: 12, color: '#A8AAB0' }}>{item.waktu}</Text>
                                <View style={{ marginTop: '5%', flex: 1, borderLeftWidth: 1.5, alignSelf: 'center', borderLeftColor: '#A8AAB0' }} />
                            </View>
                            <View style={{ flex: 2, backgroundColor: '#EEEFF2', borderRadius: 10, padding: 10, marginTop: 10 }}>
                                <Text style={{ marginBottom: 10, fontWeight: 'bold', color: '#28B0FD' }}>{item.judul}</Text>
                                <Text style={{ color: '#A8AAB0' }}>{item.deskripsi}</Text>
                            </View>
                        </View>
                    );
                }}
            />
        </View>
    );

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
        third: ThirdRoute,
    });

    const renderTabBar = (props) => (
        <TabBar
            {...props}
            getLabelText={({ route }) => route.title}
            indicatorStyle={{ backgroundColor: '#24305A' }}
            activeColor="#24305A"
            inactiveColor="grey"
            style={{ backgroundColor: 'white', borderBottomWidth: .5, borderColor: '#b3b3b3ff' }}
        />
    );

    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC' }}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.boxHeader}>
                    <Text style={styles.boxHeadertxt1}>Selamat Datang di Aplikasi{'\n'}KEPOKAPE</Text>
                    <Text style={styles.boxHeadertxt2}>Aplikasi KEPOKAPE ini diharapkan dapat{'\n'}mempermudah Administrasi dalam pelaksanaan{'\n'}matakuliah Kerja Praktek</Text>
                </View>
                <View style={styles.boxWhite}>
                    <Text style={{ color: '#24305A', fontSize: 16, marginVertical: '5%' }}>List Instansi</Text>
                    <TextInput placeholder='Search' placeholderTextColor="grey" style={{ marginBottom:'5%',borderWidth: 1, width: '60%', alignSelf: 'center', paddingVertical: 0, borderColor: '#A8AAB0' }} />
                    
                    <ScrollView>
                    <ScrollView horizontal={true}>
                        <Table>
                            <Row data={tableHead} widthArr={[50, 200, 350, 200]} style={styles.head} textStyle={styles.textHead} />

                            {
                                tableData.map((rowData, index) => (
                                    <Row
                                        key={index}
                                        data={rowData}
                                        style={[index % 2 && { backgroundColor: '#EFEEEF' }]}
                                        textStyle={styles.text}
                                        widthArr={[50, 200, 350, 200]}
                                    />
                                ))
                            }

                        </Table>

                    </ScrollView>

                </ScrollView>
                </View>
                <View style={styles.boxWhite2}>
                    <Text style={{ color: '#24305A', fontSize: 16, marginVertical: '5%', marginLeft: '5%' }}>Timeline KP</Text>


                    <View style={{ height: 500, marginHorizontal: '4%' }}>

                        <TabView
                            renderTabBar={renderTabBar}
                            navigationState={{ index, routes }}
                            renderScene={renderScene}
                            onIndexChange={setIndex}
                        />
                    </View>

                </View>
                <View style={{ paddingHorizontal: '5%', paddingVertical: '2%', marginBottom: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                    <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                    <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    boxHeader: {
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        alignItems: 'center',
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 20,
        textAlign: 'center',

    },
    boxHeadertxt2: {
        color: '#FFFFFF',
        fontSize: 14,
        textAlign: 'center',
        marginTop: '3%',
    },
    boxWhite: {
        backgroundColor: '#FFFFFF',
        marginHorizontal: '3%',
        marginTop: '5%',
        padding:16,
        paddingTop:20,
        borderRadius: 10,
        marginBottom: '5%',
        

    },
    pad: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        marginTop: '15%',
        alignSelf: 'center',
        height: '85%'
    
},
    boxWhite2: {
        backgroundColor: '#FFFFFF',
        marginHorizontal: '3%',
        paddingVertical: '4%',
        borderRadius: 10,
        marginBottom: '5%',
    },
    tableInstansi: {
        flex: 1,
        marginHorizontal: 10,
        paddingTop: 30,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    head: {
        height: 40,
        backgroundColor: '#24305A',
        alignItems:'center'
    },
    text: {
        margin: 6,
        color: 'black',
        
        marginBottom: 10
    },
    textHead: {
        color: 'white',
        margin:6
    },
    btnNumber: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: '5%'

    },

});

export default App;