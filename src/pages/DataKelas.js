import React, { Component, useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    View,
    Image
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, TableWrapper, Row, Rows, Cell } from 'react-native-table-component';
import axios from 'axios';
import { config } from '../config';

const App = (props) => {
    const { navigation } = props;
    const tableHead = ['No.', 'Prodi', 'Nama Kelas','Angkatan','Kode Dosen Akademik', 'Opsi'];
    const [tableData, setTableData] = useState([
        ['No.', 'Prodi', 'Nama Kelas', 'Angkatan','Dosen Akademik','Opsi']
    ]);
    const [table, setTable] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/data-kelas',
            data: {
                
            }
        }).then((response) => {
            var data = response.data.data;
            var temp = [];
            data.forEach((item, index) => {
                var row = [index+1, item.nama_jurusan, item.nama_kelas, item.angkatan_kelas, item.nama_dosen_pemb,element(item.nama_jurusan, item.nama_kelas,item.angkatan_kelas,item.nama_dosen_pemb)];
                temp.push(row);
            });
            setTableData(temp);
            setTable(true);
        });
    }

    const _alertIndex = (id_kelas,nama_jurusan,nama_kelas,angkatan_kelas,nama_dosen_pemb) => {
        Alert.alert(
            'Delete',
            nama_kelas,
            [
                { text: 'Cancel', onPress: () => { } },
                { text: 'OK', onPress: () => console.log("OK") },
            ],
            { cancelable: false }
        )
    }

    const element = (id_kelas,nama_jurusan,nama_kelas,angkatan_kelas,nama_dosen_pemb) => (
        <TouchableOpacity onPress={() => _alertIndex(id_kelas,nama_jurusan,nama_kelas,angkatan_kelas,nama_dosen_pemb)}>
            <View style={styles.btn}>
                <Text style={styles.btnText}>Delete</Text>
            </View>
        </TouchableOpacity>
    );
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC', flex: 1 }}>
            <View style={styles.boxHeader}>
                <Text style={styles.boxHeadertxt1}>Dashboard {'>'} Data Kelas</Text>
            </View>
            <View style={styles.boxBottom}>
                <View style={styles.headerboxWhite}>
                    <Text style={{ fontSize:16,color:'#24305A', alignSelf: 'center' }}>Data Kelas</Text>
                    <View style={{marginHorizontal:'10%',marginVertical:10 }}>
                    <Button
                        //onPress={() => handleSignIn()}
                        title="Tambah Kelas"
                        color="#24305A"
                    />
                    </View>
                    <View><TextInput placeholder='Search' placeholderTextColor="grey" style={{ alignSelf:'center',borderWidth: 1, width: '80%', paddingVertical: 0, borderColor: '#A8AAB0' }} /></View>
                </View>

                <ScrollView>
                    <ScrollView horizontal={true}>
                        <Table>
                            <Row data={tableHead} widthArr={[70, 120, 120, 110, 267, 80]} style={styles.head} textStyle={styles.textHead} />

                            {
                                tableData.map((rowData, index) => (
                                    <Row
                                        key={index}
                                        data={rowData}
                                        style={[index % 2 && { backgroundColor: '#EFEEEF' }]}
                                        textStyle={styles.text}
                                        widthArr={[50, 150, 120, 100, 250, 80]}
                                    />
                                ))
                            }

                        </Table>

                    </ScrollView>

                </ScrollView>
            </View>
            <View style={{ position: 'relative', paddingHorizontal: '5%', paddingVertical: '2%', marginTop: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    textData: {
        alignSelf: 'center',
        marginBottom: 10,
        color: '#24305A'
    },
    boxHeader: {
        zIndex: -1,
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        marginBottom: '122%'
    },
    boxBottom: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        marginTop: '15%',
        alignSelf: 'center',
        height: '85%'
    },
    head: {
        height: 40,
        backgroundColor: '#24305A',
        alignItems:'center',
    },
    text: {
        margin: 6,
        color: 'black',
        textAlign:'left',
        marginBottom: 10
    },
    textHead: {
        color: 'white',
        textAlign: 'justify',
        marginLeft:6
    },
    btn: {
        width: 58, height: 20, backgroundColor: '#24305A', borderRadius: 2,
        alignSelf: 'center'
    },
    btnText: {
        textAlign: 'center', color: 'white'
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 16,
        marginLeft: '5%',
        marginBottom: '30%',
        marginTop: '2%'
    },
    headerboxWhite: {
        marginBottom: 20
    }
});

export default App;