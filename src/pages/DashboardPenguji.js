
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, Row, Rows } from 'react-native-table-component';
import axios from 'axios';
import { config } from '../config';
import { connect } from 'react-redux';

const screen = Dimensions.get("window");
const hp = screen.height;
const wp = screen.width;

const App = (props) => {
    const { navigation, dataUser } = props;
    const tableHead = ['NIM', 'Nama'];
    const [tableData, setTableData] = useState();
    const [nilaiDone, setnilaiDone] = useState();
    const [nilaiNone, setnilaiNone] = useState();

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url + '/index.php/api/mahasiswa-all-penguji',
            data: 'id_dosen_penguji=' + dataUser.id_dosen_penguji

        }).then((response) => {
            var data = response.data.data;
            var temp = [];


            data.forEach(item => {
                var row = [item.nim_mahasiswa, item.nama_lengkap_mahasiswa];
                temp.push(row);
            });
            setTableData(temp);

        });

        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url + '/index.php/api/sum-penguji',
            data: 'id_dosen_penguji=' + dataUser.id_dosen_penguji
        }).then((response) => {
            var data1 = response.data.data1;
            var data2 = response.data.data2;

            setnilaiDone(data1)
            setnilaiNone(data2)

        });

    }
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC' }}>
            <ScrollView>
                <View style={styles.boxHeader}>
                    <Text style={styles.boxHeadertxt1}>Selamat Datang di Aplikasi{'\n'}KEPOKAPE</Text>
                    <Text style={styles.boxHeadertxt2}>Aplikasi KEPOKAPE ini diharapkan dapat{'\n'}mempermudah Administrasi dalam pelaksanaan{'\n'}matakuliah Kerja Praktek</Text>
                </View>

                <View style={styles.boxGreen}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../images/report.png')} style={{ width: wp / 6, height: wp / 6 }} resizeMode="contain" />
                        </View>
                        <View style={{ flex: 2.5 }}>
                            <Text style={styles.boxGreentxt1}>{nilaiDone}</Text>
                            <Text style={styles.boxGreentxt2}>Mahasiswa <Text style={{ fontWeight: 'bold' }}>telah</Text> di nilai</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.boxRed}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../images/scores.png')} style={{ width: wp / 6, height: wp / 6 }} resizeMode="contain" />
                        </View>
                        <View style={{ flex: 2.5 }}>
                            <Text style={styles.boxRedtxt1}>{nilaiNone}</Text>
                            <Text style={styles.boxRedtxt2}>Mahasiswa <Text style={{ fontWeight: 'bold' }}>belum</Text> di nilai</Text>
                        </View>
                    </View>
                </View>

                <TouchableOpacity onPress={()=>getData()}>
                    <View style={{backgroundColor:'#24305A',alignItems:'center',paddingVertical:10, marginHorizontal:'3%', borderRadius:10, marginTop:'5%'}}>
                        <Text  style={{color:'white', fontSize:16}}>Refresh</Text>
                    </View>
                </TouchableOpacity>

                <View style={styles.boxWhite}>
                    <Text style={{ color: '#24305A', fontSize: 20, alignSelf: 'center', marginVertical: '5%' }}>Data Mahasiswa</Text>

                    <TextInput placeholder='Search' placeholderTextColor="grey" style={{ borderWidth: 1, width: '60%', alignSelf: 'center', paddingVertical: 0, borderColor: '#A8AAB0' }} />

                    <ScrollView horizontal={true} style={{ marginHorizontal: '3%', marginTop: '5%' }}>
                        <View style={{ width: wp * 1 }}>
                            <Table borderStyle={{ borderWidth: 2, borderColor: '#24305A' }}>
                                <Row data={tableHead} style={{ backgroundColor: '#24305A', height: 40 }} textStyle={{ color: 'white', alignSelf: 'center' }} />
                                <Rows data={tableData} textStyle={{ marginLeft: '2%', marginVertical: '2%' }} />
                            </Table>
                        </View>
                    </ScrollView>

                </View>

                <View style={{ paddingHorizontal: '5%', paddingVertical: '2%', marginBottom: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                    <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                    <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
                </View>


            </ScrollView>

        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    boxHeader: {
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        alignItems: 'center',
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 20,
        textAlign: 'center',

    },
    boxHeadertxt2: {
        color: '#FFFFFF',
        fontSize: 14,
        textAlign: 'center',
        marginTop: '3%',
    },
    boxWhite: {
        backgroundColor: '#FFFFFF',
        marginHorizontal: '3%',
        marginTop: '5%',
        paddingVertical: '4%',
        borderRadius: 10,
        marginBottom: '5%',
    },
    kontenboxWhite: {
        paddingHorizontal: '4%',
        marginTop: '6%',
    },
    boxGreen: {
        marginHorizontal: '3%',
        paddingVertical: '5%',
        backgroundColor: '#72CA95',
        marginTop: '5%',
        borderRadius: 10,
    },
    boxRed: {
        marginHorizontal: '3%',
        paddingVertical: '5%',
        backgroundColor: '#FF0000',
        marginTop: '5%',
        borderRadius: 10,
    },

    boxBlue: {
        marginHorizontal: '3%',
        paddingVertical: '3%',
        backgroundColor: '#24305A',
        marginTop: '5%',
        borderRadius: 10,
        alignItems: 'center'
    },
    boxGreentxt1: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },
    boxGreentxt2: {
        fontSize: 16,
        color: '#FFFFFF',

    },
    boxRedtxt1: {
        fontSize: 20,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },
    boxRedtxt2: {
        fontSize: 16,
        color: '#FFFFFF',
    }
});

const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});

export default connect(
    reduxState
)(App);
