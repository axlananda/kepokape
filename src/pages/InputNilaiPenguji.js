import React, { useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Alert,
    _ScrollView
} from 'react-native';
import { useRoute } from "@react-navigation/native";
import { Colors } from 'react-native/Libraries/NewAppScreen';
import axios from 'axios';
import { config } from '../config';

const App = (props) => {
    const submitButtonAlert = () =>
        Alert.alert(
            "REMINDER!",
            "Anda yakin dengan data yang di-inputkan?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: 'cancel'
                },
                {
                    text: "OK", onPress: () => navigation.navigate("NilaiAkademik"),
                },

            ],
        );
    const { navigation } = props;
    const { params } = useRoute();
    const [clo_2_upkp, setclo_2_upkp] = useState();
    const [clo_4a_upkp, setclo_4a_upkp] = useState();
    const [clo_4b_upkp, setclo_4b_upkp] = useState();
    const handleSubmit = () => {
        var clo_4_upkp = ((parseInt(clo_4a_upkp) + parseInt(clo_4b_upkp)));
        axios({
            method: 'post',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            url: config.system.url+'/index.php/api/input-nilai-upkp',
            data: 'nim=' + params.nim + '&clo_2_upkp=' + clo_2_upkp + '&clo_4_upkp=' + clo_4_upkp
        }).then((response) => {
            var data = response.data;
            console.log(data);
            if(data.success=='1'){
                Alert.alert('Input Nilai Berhasil');
                navigation.goBack();
            }
        });

    }
    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.boxBottom}>
                    <View style={styles.headerboxWhite}>
                        <Text style={styles.judul}>INPUT NILAI MAHASISWA</Text>
                    </View>
                    <View style={styles.boxProfil}>
                        <Image style={{ width: 100, height: 100, marginVertical: 10 }} source={require("../images/profil.png")} />
                        <Text>{params.nama}</Text>
                        <Text style={{ marginBottom: 10 }}>{params.nim}</Text>
                    </View>
                    <View style={styles.boxNilai}>
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Cource Learning Outcome (CLO)</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text>CLO 2 :</Text>
                        <Text>Memahami jenis pekerjaan dan permasalahan tempat KP yang berkaitan dengan kompetensi program studi masing-masing.</Text>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Aspek Penilaian</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text style={{ alignSelf: 'center' }}>Penguasaan terhadap permasalahan Kerja Praktik</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Indikator</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text>Mahasiswa mampu menyajikan masalah yang dihadapi di tempat KP secara jelas dan memberikan solusi terhadap masalah tersebut.</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Kriteria</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Kurang</Text>
                                    <Text>0-20</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Cukup</Text>
                                    <Text>21-35</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Baik</Text>
                                    <Text>36-50</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Nilai</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TextInput onChangeText={(text) => setclo_2_upkp(text)} placeholder='...' keyboardType='numeric' placeholderTextColor='grey' style={{ borderWidth: 1, textAlign: 'center',color:'black' }} />
                            </View>
                            <View />
                        </View>
                    </View>

                    <View style={styles.boxNilai}>
                        <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Cource Learning Outcome (CLO)</Text>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <Text>CLO 4 :</Text>
                        <Text>Mampu berpikir kritis dalam melihat permasalahan tersebut dan memberikan solusi dengan cara membandingkan, mencocokkan, menghubungkan dengan teori-teori dan konsep-konsep yang telah dipelajari di bangku perkuliahan dan menyusunnya sebagai laporan kegiatan dan mempresentasikannya.</Text>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Aspek Penilaian 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Aspek Penilaian 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text>Isi dan Sistematika Pelaporan Kerja Praktik</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                                <Text>Teknik Presentasi</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Indikator 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Indikator 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text>Mahasiswa mampu menyajikan laporan KP secara komprehensif sesuai dengan latar belakang keilmuannya.
                                    Laporan KP mengandung teori-teori ilmiah yang diperoleh di bangku kuliah dan diimplementasikan dalam kegiatan KP.</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                                <Text>Mahasiswa dapat mempresentasikan laporan KP secara jelas dan lengkap dalam batas waktu yang diberikan, dengan menggunakan teknik presentasi yang baik dan benar.</Text>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Kriteria 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Kriteria 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Kurang</Text>
                                    <Text>0-15</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Cukup</Text>
                                    <Text>16-23</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Baik</Text>
                                    <Text>24-30</Text>
                                </View>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Kurang</Text>
                                    <Text>0-10</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Cukup</Text>
                                    <Text>11-15</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>Baik</Text>
                                    <Text>16-20</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Nilai 1:</Text>
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                            <Text style={{ alignSelf: 'center', fontWeight: 'bold' }}>Nilai 2:</Text>
                            </View>
                        </View>
                        <View style={{ borderBottomWidth: 1, marginVertical: 5 }} />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TextInput onChangeText={(text) => setclo_4a_upkp(text)} placeholder='...' keyboardType='numeric' placeholderTextColor='grey' style={{ borderWidth: 1, textAlign: 'center',color:'black' }} />
                            </View>
                            <View style={{ borderLeftWidth: 1, marginHorizontal: 5 }} />
                            <View style={{ flex: 1 }}>
                                <TextInput onChangeText={(text) => setclo_4b_upkp(text)} placeholder='...' keyboardType='numeric' placeholderTextColor='grey' style={{ borderWidth: 1, textAlign: 'center',color:'black' }} />
                            </View>
                        </View>
                    </View>




                    <View style={styles.btnSubmit}>
                        <Button title={"Submit"} onPress={handleSubmit} color="#24305A" />
                    </View>
                </View>
                <View style={{ paddingHorizontal: '5%', paddingVertical: '2%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                    <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                    <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
                </View>


            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({

    judul: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#24305A',
        marginBottom: '5%',
        borderBottomWidth: 2,
        paddingBottom: 5,
    },
    boxBottom: {
        padding: 16,
        backgroundColor: '#fff',
        borderRadius: 10,
        width: '94%',
        margin: '5%',
        alignSelf: 'center',

    },
    headerboxWhite: {
        alignItems: 'center'
    },
    boxProfil: {
        borderWidth: 1,
        borderRadius: 30,
        borderColor: 'black',
        marginHorizontal: '15%',
        marginTop: '5%',
        alignItems: 'center'
    },
    boxNilai: {
        borderWidth: 1,
        borderColor: 'black',
        marginTop: '10%',
        padding: '2%'
    },
    btnSubmit: {
        flex: 1,
        alignItems: "center",
        marginVertical: '3%'


    }
});

export default App;
