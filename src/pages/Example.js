
import React from 'react';

import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    TouchableOpacity
} from 'react-native';

const App = (props) => {
    const { navigation } = props;
    return (
        <SafeAreaView >
            <ScrollView horizontal={true}>
                <Text>Example Screen</Text>
                <ScrollView>
                    <View style={styles.headerContent}>
                        <TouchableOpacity style={styles.getStart}>
                            <Text>Get Started</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({

    headerContent: {
        marginTop: 10,
        marginHorizontal: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },

    getStart: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: 'red',


    }
});

export default App;
