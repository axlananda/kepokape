import React, { Component, useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    View,
    Image
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, TableWrapper, Row, Rows, Cell } from 'react-native-table-component';
import axios from 'axios';
import { connect } from 'react-redux';
import {config} from '../config';


const App = (props) => {
    const { navigation,dataUser } = props;
    const tableHead = ['No.', 'Nama', 'Nim', 'Prodi', 'Opsi', 'Status Nilai'];
    const [tableData, setTableData] = useState([
        ['No.', 'Nama', 'NIM', 'Prodi', 'Opsi', 'Status Nilai']
    ]);
    const [table, setTable] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/nilai-akademik',
            data: 'id_dosen_pemb=' + dataUser.id_dosen_pemb
        }).then((response) => {
            var data = response.data.data;
            var temp = [];
            data.forEach((item,index) => {
                var row = [index+1, item.nama_lengkap_mahasiswa, item.nim_mahasiswa, "Teknik Komputer", item.status_nilai=="Belum" 
                ?element(item.nim_mahasiswa, item.nama_lengkap_mahasiswa):null,item.status_nilai];
                temp.push(row);
            });
            setTableData(temp);
            setTable(true);
        });
    }

    const _alertIndex = (nim, nama) => {
        Alert.alert(
            'Input Nilai',
            nama,
            [
                { text: 'Cancel', onPress: () => { } },
                { text: 'OK', onPress: () => navigation.navigate('InputNilaiAkademik', { nim: nim, nama: nama }) },
            ],
            { cancelable: false }
        )
    }

    const element = (nim, nama) => (
        <TouchableOpacity onPress={() => _alertIndex(nim, nama)}>
            <View style={styles.btn}>
                <Text style={styles.btnText}>Pilih</Text>
            </View>
        </TouchableOpacity>
    );
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC', flex: 1 }}>
            <View style={styles.boxHeader}>
                <Text style={styles.boxHeadertxt1}>Dashboard {'>'} Nilai</Text>
            </View>
            <View style={styles.boxBottom}>
                <View style={styles.headerboxWhite}>
                    <View style={styles.textData}><Text>Data Mahasiswa</Text></View>
                    <View style={{ flex: 1 }}><TextInput placeholder='Search' placeholderTextColor="grey" style={{ borderWidth: 1, width: '100%', paddingVertical: 0, borderColor: '#A8AAB0' }} /></View>
                </View>
                
                <ScrollView>
                    <ScrollView horizontal={true}>
                        <Table>
                            <Row data={tableHead} widthArr={[50, 300, 150, 150, 100, 100]} style={styles.head} textStyle={styles.textHead} />

                            {
                                tableData.map((rowData, index) => (
                                    <Row
                                        key={index}
                                        data={rowData}
                                        style={[index % 2 && { backgroundColor: '#EFEEEF' }]}
                                        textStyle={styles.text}
                                        widthArr={[50, 300, 150, 150, 100,100]}
                                    />
                                ))
                            }
                            
                        </Table>
                        
                    </ScrollView>
                    
                </ScrollView>
                <TouchableOpacity onPress={()=>getData()}>
                    <View style={{backgroundColor:'#24305A',alignItems:'center',paddingVertical:10}}>
                        <Text  style={{color:'white'}}>Refresh</Text>
                    </View>
                </TouchableOpacity>
                
            </View>
            <View style={{position:'absolute',width:'100%',bottom:0,paddingHorizontal:'5%',paddingVertical:'2%',flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                        <Text style={{color:'#A8AAB0'}}>2021 | Telkom University</Text>
                        <Text style={{color:'#A8AAB0',fontWeight:'bold'}}>KEPO KAPE</Text>
                </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    textData:{
        flex: 1.2,
        fontWeight:'bold',
        color:'#24305A'
    },
    boxHeader: {
        zIndex: -1,
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        marginBottom:'122%'
    },
    boxBottom: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        marginTop: '15%',
        alignSelf: 'center',
        height: '85%'
    },
    head: {
        height: 40,
        backgroundColor: '#24305A'
    },
    text: {
        margin: 6,
        color: 'black',
        alignSelf: 'center',
        marginBottom: 10
    },
    textHead: {
        color: 'white',
        alignSelf: 'center'
    },
    btn: {
        width: 58, height: 20, backgroundColor: '#24305A', borderRadius: 2,
        alignSelf: 'center'
    },
    btnText: {
        textAlign: 'center', color: 'white'
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 16,
        marginLeft: '5%',
        marginBottom: '30%',
        marginTop: '2%'
    },
    headerboxWhite: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20
    }
});

const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});

export default connect(
    reduxState
)(App);