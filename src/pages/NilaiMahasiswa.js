import React, { Component, useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    View,
    Image
} from 'react-native';
import * as Progress from 'react-native-progress';
import { Table, TableWrapper, Row, Rows, Cell } from 'react-native-table-component';
import axios from 'axios';
import { useRoute } from "@react-navigation/native";
import { connect } from 'react-redux';
import { config } from '../config';

const App = (props) => {
    const { navigation,dataUser } = props;
    const { params } = useRoute();
    const [nama, setnama] = useState();

    //akademik
    const [nilai_akd, setnilai_akd] = useState();

    //upkp
    const [nilai_upkp, setnilai_upkp] = useState();

    //pbbl
    const [nilai_pbbl, setnilai_pbbl] = useState();

    //nilai akhir
    const [nilai_akhir, setnilai_akhir] = useState();

    useEffect(() => {
        getData();
        //console.log(dataUser)
    }, []);
    const getData = () => {
        axios({
            method: 'post',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            url: config.system.url+'/index.php/api/nilai-mahasiswa',
            data: 'nim=' + dataUser.nim_mahasiswa

        }).then((response) => {
            var data = response.data.data;
            
            //akademik
            
            var clo_1_akd = data.clo_1_akd;
            var clo_4_akd = data.clo_4_akd;

            //upkp
            var clo_2_upkp = data.clo_2_upkp;
            var clo_4_upkp = data.clo_4_upkp;

            //pbbl
            var clo_1_pbbl = data.clo_1_pbbl;
            var clo_2_pbbl = data.clo_2_pbbl;
            var clo_3_pbbl = data.clo_3_pbbl;
            var clo_4_pbbl = data.clo_4_pbbl;

            var nilai_akhir = data.nilai_akhir;

            //Rumus nilai akhir mahasiswa
            var nilai_akd = (Number(clo_1_akd) + Number(clo_4_akd));
            var nilai_upkp = (Number(clo_2_upkp) + Number(clo_4_upkp));
            var nilai_pbbl = (Number(clo_1_pbbl) + Number(clo_2_pbbl) + Number(clo_3_pbbl) + Number(clo_4_pbbl));
            var nilai_akhir = ((Number(nilai_akd) * 40/100 + Number(nilai_upkp) * 20/100 + Number(nilai_pbbl) * 40/100));

            setnilai_akd(nilai_akd);
            setnilai_upkp(nilai_upkp);        
            setnilai_pbbl(nilai_pbbl);
            setnilai_akhir(nilai_akhir);
        });
    }
    return (
        <SafeAreaView style={{ backgroundColor: '#DCDCDC', flex: 1 }}>
            <View style={styles.boxHeader}>
                <Text style={styles.boxHeadertxt1}>Dashboard {'>'} Nilai</Text>
            </View>
            <View style={styles.boxBottom}>
                <View style={styles.headerboxWhite}>
                    <Text style={styles.textData}>LIHAT NILAI KERJA PRAKTEK</Text>
                </View>

                <View>
                    <View style={{ backgroundColor: '#24305A', alignItems: 'center', paddingVertical: 5 }}>
                        <Text style={{ color: 'white' }}>
                            Pembimbing Akademik
                        </Text>
                    </View>
                    <View style={{ alignItems: 'center', paddingVertical: 5, borderWidth: 1 }}>
                        <Text style={{ color: '#24305A' }}>
                            {nilai_akd}
                        </Text>
                    </View>
                </View>

                <View style={{ marginTop: 20 }}>
                    <View style={{ backgroundColor: '#24305A', alignItems: 'center', paddingVertical: 5 }}>
                        <Text style={{ color: 'white' }}>
                            Pembimbing Lapangan
                        </Text>
                    </View>
                    <View style={{ alignItems: 'center', paddingVertical: 5, borderWidth: 1 }}>
                        <Text style={{ color: '#24305A' }}>
                            {nilai_pbbl}
                        </Text>
                    </View>
                </View>

                <View style={{ marginTop: 20 }}>
                    <View style={{ backgroundColor: '#24305A', alignItems: 'center', paddingVertical: 5 }}>
                        <Text style={{ color: 'white' }}>
                            Ujian Presentasi Kerja Praktek (UPKP)
                        </Text>
                    </View>
                    <View style={{ alignItems: 'center', paddingVertical: 5, borderWidth: 1 }}>
                        <Text style={{ color: '#24305A' }}>
                            {nilai_upkp}
                        </Text>
                    </View>
                </View>

                <View style={{ marginTop: 20 }}>
                    <View style={{ backgroundColor: '#24305A', alignItems: 'center', paddingVertical: 5 }}>
                        <Text style={{ color: 'white' }}>
                            Nilai Akhir
                        </Text>
                    </View>
                    <View style={{ alignItems: 'center', paddingVertical: 5, borderWidth: 1 }}>
                        <Text style={{ color: '#24305A' }}>
                            {nilai_akhir}
                        </Text>
                    </View>
                </View>

            </View>
            <View style={{ position: 'absolute',bottom:0, paddingHorizontal: '5%', paddingVertical: '2%', marginTop: '15%', width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white' }}>
                <Text style={{ color: '#A8AAB0' }}>2021 | Telkom University</Text>
                <Text style={{ color: '#A8AAB0', fontWeight: 'bold' }}>KEPO KAPE</Text>
            </View>
        </SafeAreaView>
    )
};


const styles = StyleSheet.create({
    textData: {
        color: '#24305A',
        fontSize: 16,
        borderBottomWidth: 2
    },
    boxHeader: {
        zIndex: -1,
        paddingBottom: '5%',
        backgroundColor: '#24305A',
        marginBottom: '122%'
    },
    boxBottom: {
        padding: 16,
        paddingTop: 20,
        backgroundColor: '#fff',
        borderRadius: 10,
        zIndex: 3,
        position: 'absolute',
        width: '94%',
        marginTop: '15%',
        alignSelf: 'center',
        height: '85%'
    },
    head: {
        height: 40,
        justifyContent: 'space-between',
        backgroundColor: '#24305A'
    },
    text: {
        margin: 10,
        color: 'black',
        alignSelf: 'center',
        marginBottom: 10,
    },
    textHead: {
        color: 'white',
        alignSelf: 'center',

    },
    btn: {
        width: 58, height: 20, backgroundColor: '#24305A', borderRadius: 2,
        alignSelf: 'center'
    },
    btnText: {
        textAlign: 'center', color: 'white'
    },
    boxHeadertxt1: {
        color: '#FFFFFF',
        fontSize: 16,
        marginLeft: '5%',
        marginBottom: '30%',
        marginTop: '2%'
    },
    headerboxWhite: {
        alignItems: 'center',
        marginBottom: 20
    }
});

const reduxState = state => ({
    dataUser: state.userDataReducer.userData,
});


export default connect(
    reduxState
)(App);