import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../pages/Login';
import HomeAkademik from '../pages/HomeAkademik';
import HomeLAA from '../pages/HomeLAA';
import HomeMahasiswa from '../pages/HomeMahasiswa';
import HomePenguji from '../pages/HomePenguji';
import HomeLapangan from '../pages/HomeLapangan';
import InputNilaiAkademik from '../pages/InputNilaiAkademik';
import InputNilaiPenguji from '../pages/InputNilaiPenguji';
import InputNilaiLapangan from '../pages/InputNilaiLapangan';
import HasilInputNilaiAkademik from '../pages/HasilInputNilaiAkademik';
import HasilInputNilaiPenguji from '../pages/HasilInputNilaiPenguji';
import HasilInputNilaiLapangan from '../pages/HasilInputNilaiLapangan';
import EditNilaiAkademik from '../pages/EditNilaiMahasiswa';
import EditNilaiPenguji from '../pages/EditNilaiPenguji';
import EditNilaiLapangan from '../pages/EditNilaiLapangan';
import LihatNilaiAkademik from '../pages/LihatNilaiAkademik';
import LihatNilaiPenguji from '../pages/LihatNilaiPenguji';
import LihatNilaiLapangan from '../pages/LihatNilaiLapangan';
import Splash from '../pages/Splash'

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Splash">
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
      <Stack.Screen
        name="HomeAkademik"
        component={HomeAkademik}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
      <Stack.Screen
        name="HomeLAA"
        component={HomeLAA}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
      <Stack.Screen
        name="HomeMahasiswa"
        component={HomeMahasiswa}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
      <Stack.Screen
        name="HomePenguji"
        component={HomePenguji}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
      <Stack.Screen
        name="HomeLapangan"
        component={HomeLapangan}
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
      />
      <Stack.Screen
        name="InputNilaiAkademik"
        component={InputNilaiAkademik}
        options={{
          title: 'Input Nilai',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />

      <Stack.Screen
        name="InputNilaiPenguji"
        component={InputNilaiPenguji}
        options={{
          title: 'Input Nilai',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />

      <Stack.Screen
        name="InputNilaiLapangan"
        component={InputNilaiLapangan}
        options={{
          title: 'Input Nilai',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />
      <Stack.Screen
        name="HasilInputNilaiAkademik"
        component={HasilInputNilaiAkademik}
        options={{
          title: 'Lihat Hasil Input Nilai',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />

      <Stack.Screen
        name="HasilInputNilaiPenguji"
        component={HasilInputNilaiPenguji}
        options={{
          title: 'Lihat Hasil Input Nilai',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />
      <Stack.Screen
        name="HasilInputNilaiLapangan"
        component={HasilInputNilaiLapangan}
        options={{
          title: 'Lihat Hasil Input Nilai',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />


      <Stack.Screen
        name="EditNilaiPenguji"
        component={EditNilaiPenguji}
        options={{
          title: 'Edit Nilai Penguji',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />

      <Stack.Screen
        name="EditNilaiLapangan"
        component={EditNilaiLapangan}
        options={{
          title: 'Edit Nilai Lapangan',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />

      <Stack.Screen
        name="EditNilaiMahasiswa"
        component={EditNilaiAkademik}
        options={{
          title: 'Edit Nilai Akademik',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />
      <Stack.Screen
        name="LihatInputAkademik"
        component={LihatNilaiAkademik}
        options={{
          title: 'Lihat Hasil Input Nilai',
          gestureEnabled: false,
          headerTintColor: 'white',
          headerStyle: {
            backgroundColor: '#24305A',
          },


        }}
      />

    </Stack.Navigator>
  );
};

export default Router;
