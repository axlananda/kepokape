import {
  SAVE_DATA_USER, 
} from './reduxConstant';

const initialState = {  
  userData: {}
};

const userDataReducer = (state = initialState, action) => {
  if (action.type === SAVE_DATA_USER) {
    return {
      ...state,
      userData: action.payload.value,
    };
  }  
  return state;
};

export default userDataReducer;
