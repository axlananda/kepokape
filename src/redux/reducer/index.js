import {combineReducers} from 'redux';
import userDataReducer from '../reduxReducer';

const reducer = combineReducers({
  userDataReducer,
});

export default reducer;
