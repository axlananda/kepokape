
import {SAVE_DATA_USER} from './reduxConstant';

export const saveUserValue = payload => {
  return {
    type: SAVE_DATA_USER,
    payload: {
      type: payload.type,
      value: payload.value,
    },
  };
};

